This repository records the content of the Javascript based BasicBI - Interval Breast Cancer Assessment Risk
which is included in the cloud.decisionq.com image as a default.  This repository was created as an indirect 
response to a request to locate this application.  It was then discovered that this application was not in
a formal repository. 

This application is normally sited at /webapps/basicBI on the cloud server (cloud.decisionq.com).

