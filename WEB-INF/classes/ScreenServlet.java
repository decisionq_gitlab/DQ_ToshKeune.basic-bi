// This is ScreenServlet.java.
// ScreenServlet receives a web form of breast cancer indicators, passes
// the data to DecisioQ's breast cancer screening model, and returns the
// probability that results of a tumor biopsy indicate disease.
//
// ScreenServlet.java was written by P. Kalina at DecisionQ Corp in
// April, 2012.  It is inspired, in part, by a page at
// http://tutorialspoint.com
//
// Copyright © 2012, DecisionQ Corporation.  All rights reserved.

public class ScreenServlet extends javax.servlet.http.HttpServlet {

    private static final java.lang.String[] FIELDNAMES = {
            "cDegree",      // hard code the field names for now
            "ageAtVisit",
            "isFiveYrRiskPtGt1-66Pct",
            "CBE_palpableLesion",
            "US_cExamResults",
            "MMG_cExamResults",
            "isNulliparous",
            "mpDuration",
            "priorBiopsiesYesNo",
            "mpRegularity",
            "CBE_cExamResults",
            "race",
//            "Biopsy 3 Bin Rev",   Comment out because this is the dependent variable
            "MRI_cExamResults"
    };
    private static final java.lang.String[] DUMMYVALUES = {
            "A",      // hard code the field names for now
            "1",
            "A",
            "A",
            "A",
            "A",
            "A",
            "1",
            "A",
            "A",
            "A",
            "A",
//            "Biopsy 3 Bin Rev",   Comment out because this is the dependent variable
            "A"
    };
    private static final java.lang.String PATHTODATAFILE = "oneDataRecord.csv";
    private static final java.lang.String PATHTOMODELFILE = "breastCancerModelNoIntegers.bn5";
//    private static final java.lang.String PATHTOMODELFILE = "Breast screening test model 4.bn5";
    private static final java.lang.String PATHTOINFERENCEFILE = "inference.csv";
    private static final java.lang.String DEPENDENTVARIABLE = "Biopsy 3 Bin Rev";
    private static final java.lang.String COMMA = ",";

//  Method to handle post request.
    public void doPost(javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, java.io.IOException {
        System.out.println("pkalina: user.dir evaluates to "+System.getProperty("user.dir"));
            // not saving result of getProperty -- just writing to log file to study its function in servlet context
        doGet(request, response);
    }

//  Method to handle get request.
    public void doGet(javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, java.io.IOException {

        response.setContentType("text/html");

        String title = "Interval Breast Cancer Risk Assessment";
        String docType =
        "<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n";

        java.io.File oldFile;
        // delete any old instances of inference file
        oldFile = new java.io.File(PATHTOINFERENCEFILE);
        if (oldFile.exists()) {
            oldFile.delete();
        }

        // delete any old instances of data file
        oldFile = new java.io.File(PATHTODATAFILE);
        if (oldFile.exists()) {
            oldFile.delete();
        }
        oldFile = null;

        java.io.BufferedWriter dataWriter;
        try {
            dataWriter = new java.io.BufferedWriter(new java.io.FileWriter(PATHTODATAFILE));
        } catch (java.io.IOException e) {
            throw new Error("Error creating data file/n/l Error occurred "+e.toString());
        }

        // Prepare FIELDNAMES and form data to be written to the data file.
        // It seems to help if DEPENDENTVARIABLE is first.
        java.lang.StringBuffer headerBuffer = new java.lang.StringBuffer(DEPENDENTVARIABLE);
        java.lang.StringBuffer dummyBuffer = new java.lang.StringBuffer("A"); //A if String or 1 if numerics
        java.lang.StringBuffer dataBuffer = new java.lang.StringBuffer();  //Value of DEPENDENTVARIABLE is null
//        for (java.lang.String fieldName : FIELDNAMES) {    // for each fieldName in FIELDNAMES
        for (int i = 0; i < FIELDNAMES.length; i++) {
            headerBuffer.append(COMMA).append(FIELDNAMES[i]);
            dummyBuffer.append(COMMA).append(DUMMYVALUES[i]);
            dataBuffer.append(COMMA).append(request.getParameter(FIELDNAMES[i]));
        }

        // write header record of data file
        try {
            dataWriter.write(headerBuffer.toString(), 0, headerBuffer.length());
            dataWriter.newLine();
        } catch (java.io.IOException e) {
            throw new Error("Error writing header of data file ");
        }
        headerBuffer = null;

//        // Commented out writing of dummy record b/c Sean's 2012May13 fix is supposed to remove need for it
//        // write dummy record of data file
//        try {
//            dataWriter.write(dummyBuffer.toString(), 0, dummyBuffer.length());
//            dataWriter.newLine();
//        } catch (java.io.IOException e) {
//            throw new Error("Error writing header of data file ");
//        }
//        dummyBuffer = null;

        // write data record of data file
        try {
            dataWriter.write(dataBuffer.toString(), 0, dataBuffer.length());
            dataWriter.newLine();
        } catch (java.io.IOException e) {
            throw new Error("Error writing data record of data file ");
        }
        dataBuffer = null;

        // close the data file we just created
        try {
            dataWriter.close();
        } catch (java.io.IOException e) {
            throw new Error("Error closing just-created data file");
        }
        dataWriter = null;

        // call batch inference to generate a prediction based on data file
        java.util.List<String> outputVars = new java.util.ArrayList<String>();
        java.util.List<String> throughVars = new java.util.ArrayList<String>();
        outputVars.add("Biopsy 3 Bin Rev");

        datadigest.inference.BatchInferenceFacade bi =
                new datadigest.inference.BatchInferenceFacade(PATHTODATAFILE,
                PATHTOMODELFILE, outputVars, throughVars);
        java.io.BufferedReader inferenceReader;
        try {
            inferenceReader = new java.io.BufferedReader(
                    new java.io.InputStreamReader(bi.runBatchInference()));
        } catch (Exception e) {
            System.err.println("Error occurred:  "+e.toString());
            throw new Error("Error during call to bi.runBatchInference()");
        }

//        temporarilly comment out the code to wait for and open inference file
//        datadigest.inference.BatchInferenceFacade bi = new datadigest.inference.BatchInferenceFacade(PATHTODATAFILE, PATHTOMODELFILE, outputVars, throughVars);
//        try {
//            bi.runBatchInference();
//        } catch (Exception e) {
//            System.err.println("Error occurred:  "+e.toString());
//            throw new Error("Error while running bi.runBatchInference()");
//        }

//        // open inference file, waiting in case it is not found
//        long accumulatedSleep = 0l;
//        long sleepThisLong = 200l;
//        long maxSleep = 60l * 1000l;        // sleep no more than sixty seconds
//        boolean inferenceFileNotFound = true;
//        java.io.BufferedReader inferenceReader = null;
//        while (inferenceFileNotFound) {
//            try {
//                Thread.sleep(sleepThisLong);
//            } catch (java.lang.InterruptedException e) {
//                System.err.println("Error occurred:  "+e.toString());
//                throw new Error("Error sleeping 'til inference predictions are retrieved");
//            }
//            accumulatedSleep += sleepThisLong;
//            try {
//                inferenceReader = new java.io.BufferedReader(new java.io.FileReader(PATHTOINFERENCEFILE));
//                inferenceFileNotFound = false;
//            } catch (java.io.IOException e) {
//                // If we've reached maxSleep throw error; else continue in sleep loop
//                if (accumulatedSleep >= maxSleep) {
//                    System.err.println("Error occurred:  "+e.toString());
//                    throw new Error("Error opening inference file");
//                }
//            }
//        }
//        System.out.println("Awake after sleep of "+accumulatedSleep+" miliseconds");

        java.lang.String inferenceHeader;
        // read header record of inference results
        try {
            inferenceHeader = inferenceReader.readLine();
        } catch (java.io.IOException e) {
            System.err.println("Error occurred:  "+e.toString());
            throw new Error("Error reading header of inference results");
        }
        System.out.println("inferenceHeader="+inferenceHeader);

        java.lang.String inferenceProbabilities;
        // read data record of inference file
        try {
//            inferenceProbabilities = inferenceReader.readLine();  //read and ignore first record as it's for the dummy values
            inferenceProbabilities = inferenceReader.readLine();
        } catch (java.io.IOException e) {
            throw new Error("Error reading inference probabilities");
        }
        System.out.println("inferenceProbabilities="+inferenceProbabilities);

        // parse the data record by the comma delimiter
        java.lang.String[] probabilities = inferenceProbabilities.split(",");
        inferenceHeader = null;
        inferenceProbabilities = null;

        // close the inference reader stream
        try {
            inferenceReader.close();
        } catch (java.io.IOException e) {
            System.err.println("Error occurred:  "+e.toString());
            throw new Error("Error closing InputStream from batch inference");
        }
        inferenceReader = null;
        bi = null;

        float predictionProbability = 1.0f - java.lang.Float.valueOf(probabilities[1]);
                            //2nd field in 2nd line of inference file is probability of benign
                            //report 1 - probability of benign

        // format the form data and the prediction to return to the user
        java.lang.StringBuffer outStringBuffer = new java.lang.StringBuffer(docType)
                .append("<html>\n")
                .append("<head>\n")
                .append("<title>").append(title).append("</title>\n")
                .append("  <link rel=\"stylesheet\" type=\"text/css\" href=\"style2.css\"/>\n")
                .append("</head>\n")
                .append("<body>\n")
                .append("<div id=\"wrapper\">\n")
                .append("<div id=\"header\">\n")
                .append("<strong class=\"logo\"><a href=\"http://decisionq.com\">DecisionQ Turning Data Into Decisions</a></strong>\n")
                .append("<div id=\"project-name\">Interval Breast Cancer Risk Assessment</div>\n")
                .append("</div> <!-- close header-container -->\n")
                .append("<div id=\"main-container\">\n")
                .append("<div id=\"left-sidebar\">\n")
                .append("<div id=\"info-container\">\n")
                .append("<p>This calculator uses information about you, your medical history, and your current health status to provide an estimate of having breast disease in the next twelve months.  This estimate does not mean that you will or will not develop disease but rather provides a guide of your risk relative to others, allowing for informed decisions to be made about additional breast cancer screening.</p>\n")
                .append("<p>These estimates are derived statistically using a study cohort of 8,500 women enrolled in three  breast cancer screening studies in five clinical centers.</p>\n")
                .append("<p>Any of the fields to the right can be left blank if the value is unknown. Please be sure to supply as much information as possible to obtain the most accurate result. Any information left blank will be calculated based on what values are most likely, given the information provided.</p>\n")
                .append("</div> <!-- close info-container -->\n")
                .append("</div> <!-- close left-sidebar -->\n")
                .append("<div id=\"result-container\">\n")
                .append("<h2>Data Summary and Prediction</h2>\n")
                .append("<p>\n")
                .append("You entered:\n")
                .append("</p>\n")
                .append("<ul>\n");

        // for each fieldName in FIELDNAMES
        for (java.lang.String fieldName : FIELDNAMES) {
            outStringBuffer.append("  <li><strong>").append(fieldName).append("</strong>: ")
                    .append(blankNotNull(request.getParameter(fieldName))).append("\n");
        }

        outStringBuffer.append("</ul>\n")
                .append("<p>\n")
                .append("Your result is:\n")
                .append("</p>\n")
                .append("<ul>\n")
                .append("  <li><strong>Predicted Likelihood of Disease</strong>:&nbsp; ");

        if (predictionProbability < .15) {
            outStringBuffer.append("Low (");
        } else {
            outStringBuffer.append("High (");
        }

        java.util.Formatter formatThis = new java.util.Formatter(outStringBuffer);
        formatThis.format("%.1f", (100.0f * predictionProbability));
        formatThis = null;

        outStringBuffer.append("%)\n")
                .append("</ul>\n")
                .append("</div> <!-- Close Result Container -->\n")
                .append("</div> <!-- Main Container -->\n")
                .append("</div> <!-- Wrapper -->\n")
                .append("<div id=\"footer\">\n")
                .append("<div id=\"foot\">\n")
                .append("<p>This is a product of DecisionQ Corporation. Copyright &copy; 2012. All Rights Reserved</p>\n")
                .append("<p>Alpha version. Please contact <a href=\"mailto:benjamin.petersen@decisionq.com?subject=[Alpha Web Interpreter]\">Benjamin Petersen</A> with any issues</p>\n")
                .append("</div>\n")
                .append("</div> <!-- close footer -->\n")
                .append("</body></html>");

        // return the form data and prediction to the user
        response.getWriter().println(outStringBuffer.toString());
        outStringBuffer = null;
    }

    // Cause the return page to say <blank> instead of nothing or null if a field is empty
    java.lang.String blankNotNull(java.lang.String paramValue) {
        java.lang.String returnValue;
        if (paramValue == null) {
            returnValue = "&lt;blank&gt;";
        } else if (paramValue.length() == 0) {
            returnValue = "&lt;blank&gt;";
        } else if (paramValue.equals("null")) {
            returnValue = "&lt;blank&gt;";
        } else {
            returnValue = paramValue;
        }
        return returnValue;
    }
}
